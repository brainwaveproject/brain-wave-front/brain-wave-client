FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run buildProd

FROM nginx:1.17-alpine

COPY --from=node /app/dist/brain-wave /usr/share/nginx/html

COPY nginx/nginx.config /etc/nginx/conf.d/default.conf

RUN adduser -D brainwave-user
USER brainwave-user

CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'
