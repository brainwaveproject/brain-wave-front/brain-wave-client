import { Component, OnInit, Renderer2 } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    private _enteredButton = false;
    isMatMenuOpen = false;

    constructor(private _renderer2: Renderer2) { }

    ngOnInit(): void {
    }

    onMouseEnterButton(trigger: MatMenuTrigger): void {
        if (!this.isMatMenuOpen) {
            trigger.openMenu();
        }

        this._enteredButton = true;
    }

    onMenuEnter(): void {
        this.isMatMenuOpen = true;
    }

    onMenuLeave(trigger: MatMenuTrigger, button: MatButton): void {
        setTimeout(() => {
            if (!this._enteredButton) {
                trigger.closeMenu();
            }

            this.isMatMenuOpen = false;
            this.removeClassesOnButton(button, 'cdk-focused', 'cdk-program-focused');
        }, 40);
    }

    onMouseLeaveButton(trigger: MatMenuTrigger, button: MatButton): void {
        setTimeout(() => {
            this._enteredButton && !this.isMatMenuOpen
                ? trigger.closeMenu()
                : this._enteredButton = false;

            this.removeClassesOnButton(button, 'cdk-focused', 'cdk-program-focused');
        }, 50);
    }

    private removeClassesOnButton(element: MatButton, ...classes: string[]): void {
        classes.forEach((cssClass: string) =>
            this._renderer2.removeClass(element._elementRef.nativeElement, cssClass));
    }
}
