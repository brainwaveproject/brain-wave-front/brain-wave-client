import { Theme } from './../../../../models/theme.enum';
import { ThemeService } from './../../../../services/ui/theme.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSlideToggle } from '@angular/material/slide-toggle';

@Component({
    selector: 'app-theme-selector',
    templateUrl: './theme-selector.component.html',
    styleUrls: ['./theme-selector.component.scss']
})
export class ThemeSelectorComponent implements OnInit {

    constructor(private _themeService: ThemeService) { }

    ngOnInit(): void {
    }

    onChangeTheme(slideToggleTheme: MatSlideToggle): void {
        slideToggleTheme.checked
            ? this._themeService.emitThemeColor(Theme.LIGHT)
            : this._themeService.emitThemeColor(Theme.DARK);
    }
}
