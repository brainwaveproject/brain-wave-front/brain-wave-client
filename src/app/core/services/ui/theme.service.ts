import { Theme } from './../../models/theme.enum';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

const THEME_SUFFIX = '-theme';

@Injectable({
    providedIn: 'root'
})
export class ThemeService {
    private themeColorSource = new BehaviorSubject<string>(Theme.LIGHT);
    themeColor$ = this.themeColorSource.asObservable();

    constructor(private _overlayContainer: OverlayContainer) { }

    emitThemeColor(themeColor: string): void {
        this.updateOverlayContainerTheme(themeColor);
        this.themeColorSource.next(themeColor);
    }

    private updateOverlayContainerTheme(themeColor: string): void {
        const overlayContainerClasses = this._overlayContainer.getContainerElement().classList;
        const themeClassesToRemove = Array
            .from(overlayContainerClasses)
            .filter((item: string) =>
                item.includes(THEME_SUFFIX)
            );

        if (themeClassesToRemove.length) {
            overlayContainerClasses.remove(...themeClassesToRemove);
        }

        overlayContainerClasses.add(themeColor);
    }
}
