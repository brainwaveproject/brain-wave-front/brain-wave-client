import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './components/header/header.component';
import { FourOhFourComponent } from './components/four-oh-four/four-oh-four.component';
import { ThemeSelectorComponent } from './components/header/nav-bar/theme-selector/theme-selector.component';
import { FooterComponent } from './components/footer/footer.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { NavbarComponent } from './components/header/nav-bar/navbar.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

const material = [
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatSlideToggleModule
];
@NgModule({
    declarations: [
        HeaderComponent,
        FourOhFourComponent,
        NavbarComponent,
        ThemeSelectorComponent,
        FooterComponent
    ],
    imports: [
        CommonModule,
        material
    ],
    exports: [
        HeaderComponent,
        FooterComponent
    ]
})
export class CoreModule { }
