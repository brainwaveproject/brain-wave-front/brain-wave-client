import { ThemeService } from './core/services/ui/theme.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    @HostBinding('class') private cssClassComponent?: string;
    private themeSubscription?: Subscription;

    constructor(private _themeService: ThemeService) { }

    ngOnInit(): void {
        this.themeSubscription =
            this._themeService.themeColor$.subscribe((themeColor: string) =>
                this.cssClassComponent = themeColor
            );
    }

    ngOnDestroy(): void {
    this.themeSubscription?.unsubscribe();
    }
}
